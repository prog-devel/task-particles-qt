import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

ColumnLayout {
    property alias min: slider.from
    property alias max: slider.to

    property vector2d value

    property string name

    Label {
        Layout.alignment: Qt.AlignHCenter

        text: qsTr("%1 [%2, %3]").arg(name).arg(Number(slider.first.value).toFixed(2)).arg(Number(slider.second.value).toFixed(2))
    }

    RangeSlider {
        id: slider

        Layout.fillWidth: true

        first.value: value.x
        second.value: value.y

        first.onValueChanged: value.x = first.value
        second.onValueChanged: value.y = second.value
    }
}

