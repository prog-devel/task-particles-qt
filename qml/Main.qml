import QtQuick
import QtQuick.Controls
import particles

Window {
    id: main

    width: 1280
    height: 800
    visible: true
    title: qsTr("Hello World")
    // color: Qt.black

    Image {
        source: "qrc:/images/bg.jpg"
        anchors.fill: parent
        z: -1
        // visible: false
    }

    Timer {
        id: tmrRestart

        repeat: true
        running: !cfgView.visible
        interval: 3000
        triggeredOnStart: true

        onTriggered: {
            ps1.restart()
            ps2.restart()
        }
    }

    ParticleSystem {
        id: ps1

        anchors.fill: parent

        shaders {
            vert: ":/particles/shaders/particles.vert.glsl"
            geom: ":/particles/shaders/particles.geom.glsl"
            frag: ":/particles/shaders/particles.frag.glsl"
            comp: ":/particles/shaders/particles.comp.glsl"
        }

        numParticles: 1024*50
        gravity: Qt.vector2d(0.0, 1.8)

        spawnTimeRange: Qt.vector2d(0.1, 0.12)
        lifeTimeRange: Qt.vector2d(0.2, 1.5)

        spawnPoint: Qt.point(-0.2, -0.2)

        spawnDirectionRange: Qt.vector2d(-Math.PI, Math.PI)
        spawnVelocityRange: Qt.vector2d(0.2, 1.0)

        particleColors: [
            Qt.rgba(0.8, 0.8, 1.0, 1.0),
            Qt.rgba(1.0, 0.0, 0.0, 1.0),
            Qt.rgba(0.0, 1.0, 0.0, 1.0),
            Qt.rgba(0.0, 0.0, 1.0, 1.0),
        ]

        particleSizeRange: Qt.vector2d(0.01, 0.05);
    }

    ParticleSystem {
        id: ps2

        anchors.fill: parent

        shaders {
            vert: ":/particles/shaders/particles.vert.glsl"
            geom: ":/particles/shaders/particles.geom.glsl"
            frag: ":/particles/shaders/particles.frag.glsl"
            comp: ":/particles/shaders/particles.comp.glsl"
        }

        numParticles: 1024*50
        gravity: Qt.vector2d(0.0, 1.8)

        spawnTimeRange: Qt.vector2d(0.1, 0.2)
        lifeTimeRange: Qt.vector2d(1.5, 1.5)

        spawnPoint: Qt.point(0.35, 0.18)

        spawnDirectionRange: Qt.vector2d(-0.5 - Math.PI/2.0, - Math.PI/2.0)
        spawnVelocityRange: Qt.vector2d(3.0, 5.0)

        particleColors: [
            Qt.rgba(1.0, 1.0, 1.0, 1.0),
        ]

        particleSizeRange: Qt.vector2d(0.005, 0.02);
    }

    FPSCounter { id: fc }

    Text {
        id: txtFPS

        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 10

        color: "white"
        font.pixelSize: 20
        text: qsTr("FPS: ") + Number(fc.fps).toFixed(2)
    }

    Button {
        id: btnConfig
        text: "Config"
        visible: !cfgView.visible

        opacity: 0.9

        onClicked: cfgView.open()
    }

    Shortcut {
        sequence: "Ctrl+P"
        onActivated: cfgView.open()
    }

    ConfigView {
        id: cfgView

        particleSystems: [
            {obj: ps1, name: qsTr("Firework 1")},
            {obj: ps2, name: qsTr("Firework 2")},
        ]
    }

    Dialog {
        id: dlgExit
        anchors.centerIn: parent

        focus: true
        modal: true
        title: qsTr("Are you sure?")
        background.opacity: 0.7
        standardButtons: Dialog.Yes | Dialog.No
        onAccepted: Qt.exit(0)

        Label {
            text: qsTr("Do you really want to close this awesome app?")
        }

        Shortcut {
            sequence: "Return"
            onActivated: dlgExit.accept()
        }
    }

    Shortcut {
        sequence: "Esc"
        onActivated: close()
    }

    onClosing: (close) => {
        dlgExit.open()
        close.accepted = false
    }
}
