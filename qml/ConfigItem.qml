import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import particles

Item {
    id: configItem
    required property ParticleSystem ps

    // color: "red"

    ColumnLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        RowLayout {
           Layout.alignment: Qt.AlignHCenter
           Layout.bottomMargin: 50

            Label {
                text: qsTr("NumParticles")
            }

            SpinBox {
                id: sldNumPart

                from: 1
                to: 1024*10000
                editable: true

                Component.onCompleted: {
                    value = ps.numParticles
                    // avoid data races on startup
                    // TODO think about it
                    bndNumPart.when = true
                }

                Binding {
                    id: bndNumPart
                    when: false
                    target: ps
                    property: "numParticles"
                    value: sldNumPart.value
                }
            }
        }

        Repeater {
            onVisibleChanged: {
                if (!visible) return;
                model = [
                            { name: "Spawn Time", min: 0.0, max: 5.0, src: ps.spawnTimeRange, srcName: "spawnTimeRange" },
                            { name: "Life Time", min: 0.1, max: 3.0, src: ps.lifeTimeRange, srcName: "lifeTimeRange" },
                            { name: "Direction", min: -Math.PI, max: Math.PI, src: ps.spawnDirectionRange, srcName: "spawnDirectionRange" },
                            { name: "Velocity", min: 0.0, max: 10, src: ps.spawnVelocityRange, srcName: "spawnVelocityRange" },
                            { name: "Particle Size", min: 0.001, max: 0.05, src: ps.particleSizeRange, srcName: "particleSizeRange" },
                        ]
            }

            MinMaxSlider {
                id: slider

                required property var modelData
                required property int index

                name: modelData.name
                min: modelData.min
                max: modelData.max
                value: modelData.src

                Component.onCompleted: {
                    // avoid data races on startup
                    // TODO think about it
                    bnd.when = true
                }

                Binding {
                    id: bnd
                    when: false
                    target: ps
                    property: modelData.srcName
                    value: slider.value
                }
            }
        }

        Button {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            text: "Bang!"

            onClicked: {
                ps.restart()
            }
        }
    }
}
