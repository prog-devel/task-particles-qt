import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import particles

Drawer {
    id: popCfg

    property list<var> particleSystems: []

    focus: true
    dim: false

    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

    width: 0.40 * parent.width
    height: parent.height

    padding: 10

    background.opacity: 0.9

    ColumnLayout {
        anchors.fill: parent

        ComboBox {
            id: cmbSelect

            Layout.fillWidth: true

            textRole: "name"
            valueRole: "obj"

            model: popCfg.particleSystems
            currentIndex: 0

            onActivated: {
                view.currentIndex = cmbSelect.currentIndex
            }
        }

        SwipeView {
            id: view

            clip: true

            Layout.fillWidth: true
            Layout.fillHeight: true

            onCurrentIndexChanged: {
                cmbSelect.currentIndex = view.currentIndex
            }

            Repeater {
                model: popCfg.particleSystems
                ConfigItem {
                    required property var modelData
                    ps: modelData.obj
                }
            }
        }

        PageIndicator {
            id: indicator

            Layout.alignment: Qt.AlignHCenter

            count: view.count
            currentIndex: view.currentIndex

        }
    }
}
