#pragma once

#include <QElapsedTimer>
#include <QQuickItem>
#include <QQuickWindow>

class QTimer;

// TODO Global counters object?
class FPSCounter : public QQuickItem
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(qreal fps READ fps NOTIFY onFPSChanged FINAL)

public:
    explicit FPSCounter(QQuickItem* parent = nullptr);

    qreal fps() const { return m_fps; }

signals:
    void onFPSChanged();

private slots:
    void handleWindowChanged(QQuickWindow *win);

private:
    QTimer* m_updateTimer;
    QElapsedTimer m_t;
    qreal m_fps = 0.0;
    qreal m_newFPS = 0.0;
    qint64 m_count = 0;
};
