#include "particles.h"

#include <vector>

#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

#include <QOpenGLShader>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLExtraFunctions>

#include "utils.h"

using namespace utils;

namespace {

constexpr int WORK_GROUP_SIZE = 128;

// memory layout is std140 (see compute shader)
struct Particle
{
    glm::vec2 lifetime;
    glm::vec2 velocity;
    glm::vec3 pos;
    glm::vec4 color alignas(16); // vec3 pos is actually vec4 on CS
    glm::vec2 size;
};
} // anonymous namespace


class ParticleSystem::Impl final: public ViewportItem::RendererImpl, protected QOpenGLExtraFunctions
{
public:
    Impl(const ParticleSystemInit&);

    void render(float tDiff) override;

private:
    std::vector<Particle> generateInitialState(const ParticleSystemInit& init);

private:
    QOpenGLShaderProgram m_progRend;
    QOpenGLShaderProgram m_progComp;

    QOpenGLBuffer m_ssboParticles;

    QOpenGLVertexArrayObject m_vaoParicles;
    QOpenGLVertexArrayObject m_indexBuf;

    QMatrix4x4 m_proj;

    // Uniform locations (compute shader)
    GLuint m_csTimeLoc = 0;

    // Uniform locations (render shaders)
    GLuint m_viewMatLoc = 0;
    GLuint m_projMatLoc = 0;

    int m_numParticles = 0;
};

ParticleSystemInit::ParticleSystemInit(QObject* outer) : m_shaders(new ShadersGroup(outer)) {}

ParticleSystem::ParticleSystem(QQuickItem *parent)
    : ViewportItem(parent, [this]() -> std::unique_ptr<ViewportItem::RendererImpl> { return std::make_unique<Impl>(this->initialState()); })
    , ParticleSystemInit(this)
{
}


ParticleSystem::Impl::Impl(const ParticleSystemInit& init)
    : m_ssboParticles{QOpenGLBuffer::VertexBuffer}
    // Pad number of particles to WORK_GROUP_SIZE
    , m_numParticles{ (init.m_numParticles + WORK_GROUP_SIZE - 1) / WORK_GROUP_SIZE * WORK_GROUP_SIZE }
    , m_proj(init.m_proj)
{
    initializeOpenGLFunctions();

    if (init.m_particleColors.empty()) {
        throw std::runtime_error("Invalid particleColors property");
    }

    /// SSBOs

    {
        if (!m_ssboParticles.create()) {
            throw std::runtime_error("Failed to create buffer");
        }

        const auto particleVerts = generateInitialState(init);

        constexpr auto BUF_SSBO = GL_SHADER_STORAGE_BUFFER;
        glBindBuffer(BUF_SSBO, m_ssboParticles.bufferId());

        const auto bufSize = particleVerts.size() * sizeof(Particle);
        glBufferData(BUF_SSBO, bufSize, NULL, GL_STATIC_DRAW);
        auto* mbuf = glMapBufferRange(BUF_SSBO, 0, bufSize, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        if (!mbuf) {
            throw std::runtime_error("Failed to initialize buffer");
        }
        memcpy(mbuf, particleVerts.data(), bufSize);
        glUnmapBuffer(BUF_SSBO);
        glBindBuffer(BUF_SSBO, 0);
    }

    /// VAO

    if (!m_vaoParicles.create()) {
        throw std::runtime_error("Failed to create VAO");
    }

    m_vaoParicles.bind();
    m_ssboParticles.bind();

    // Attrib layouts (see Particle struct and vert shader source)
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Particle), reinterpret_cast<void*>(offsetof(Particle, color)));

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), reinterpret_cast<void*>(offsetof(Particle, pos)));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Particle), reinterpret_cast<void*>(offsetof(Particle, lifetime)));

    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Particle), reinterpret_cast<void*>(offsetof(Particle, size)));

    m_vaoParicles.release();
    m_ssboParticles.release();

    /// Render SP

    if (!m_progRend.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex, init.m_shaders->vert) ||
        !m_progRend.addCacheableShaderFromSourceFile(QOpenGLShader::Geometry, init.m_shaders->geom) ||
        !m_progRend.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment, init.m_shaders->frag)) {

        qCritical() << m_progRend.log();
        throw std::runtime_error("Failed to compile shaders");
    }

    if (!m_progRend.link()) {
        qCritical() << m_progRend.log();
        throw std::runtime_error("Failed to link shaders");
    }

    m_viewMatLoc = m_progRend.uniformLocation("viewMat");
    m_projMatLoc = m_progRend.uniformLocation("projMat");

    /// Comp SP

    if (!m_progComp.addCacheableShaderFromSourceFile(QOpenGLShader::Compute, init.m_shaders->comp)) {
        qCritical() << m_progComp.log();
        throw std::runtime_error("Failed to compile shaders");
    }
    if (!m_progComp.link()) {
        qCritical() << m_progComp.log();
        throw std::runtime_error("Failed to link shaders");
    }

    m_csTimeLoc = m_progComp.uniformLocation("deltaTime");

    m_progComp.bind();
    m_progComp.setUniformValue("gravity", init.m_gravity);
    m_progComp.release();
}

std::vector<Particle> ParticleSystem::Impl::generateInitialState(const ParticleSystemInit& init)
{
    // WARNING this is bottleneck!!!
    // TODO initialize SSBO on GPU side to avoid lags on startup
    std::vector<Particle> particleVerts;
    particleVerts.reserve(m_numParticles);
    for(int i = 0; i < m_numParticles; i++) {
        const auto& color = randElement(init.m_particleColors);
        const auto size = randRange<RandDistrib::Normal>(init.m_particleSizeRange.x(), init.m_particleSizeRange.y());

        const auto rotAngle = randRange<RandDistrib::Normal>(init.m_spawnDirectionRange.x(), init.m_spawnDirectionRange.y());
        const auto modVel = glm::vec4{randRange<RandDistrib::Normal>(init.m_spawnVelocityRange.x(), init.m_spawnVelocityRange.y()), 0.0f, 0.0f, 0.0f};
        const auto velocity = glm::rotate(rotAngle, glm::vec3{0.0f, 0.0f, 1.0f}) * modVel;

        particleVerts.push_back(Particle{
            // lifetime
            {randRange<RandDistrib::Normal>(init.m_spawnTimeRange.x(), init.m_spawnTimeRange.y()),
             randRange<RandDistrib::Normal>(init.m_lifeTimeRange.x(), init.m_lifeTimeRange.y())},
            // velocity
            velocity,
            //position
            {init.m_spawnPoint.x(), init.m_spawnPoint.y(), randRange<RandDistrib::Normal>(-1.0f, 1.0f)},
            // color
            {color.redF(), color.greenF(), color.blueF(), color.alphaF()},
            // size
            {size, size}
        });
    }

    return particleVerts;
}

void ParticleSystem::Impl::render(float tDiff)
{
    /// Comp

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_ssboParticles.bufferId());
    m_progComp.bind();

    m_progComp.setUniformValue(m_csTimeLoc, tDiff);

    glDispatchCompute(std::max(m_numParticles / WORK_GROUP_SIZE, 1), 1, 1);

    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    m_progComp.release();
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    /// Render

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    m_progRend.bind();

    // TODO aspect ratio
    m_progRend.setUniformValue(m_viewMatLoc, QMatrix4x4());
    m_progRend.setUniformValue(m_projMatLoc, m_proj);

    m_ssboParticles.bind();
    m_vaoParicles.bind();

    // TODO inderect draw
    glDrawArrays(GL_POINTS, 0, m_numParticles);

    m_vaoParicles.release();
    m_ssboParticles.release();
    m_progRend.release();
}
