#pragma once

#include <cstdlib>
#include <limits>
#include <random>
#include <type_traits>

namespace utils {

enum class RandDistrib
{
    Uniform,
    Normal
};

float randNormal();

namespace detail {

template <class T, RandDistrib D, class _ = void> struct RandRange;

template <>
struct RandRange<float, RandDistrib::Uniform>
{
    static float range(float lower, float upper)
    {
        return (float)rand() / (float)RAND_MAX * (upper - lower) + lower;
    }
};

template <>
struct RandRange<float, RandDistrib::Normal>
{
    static float range(float lower, float upper)
    {
        return randNormal() * (upper - lower) + lower;
    }
};

template <class T>
struct RandRange<T, RandDistrib::Uniform, std::enable_if_t<std::numeric_limits<T>::is_integer>>
{
    static T range(T lower, T upper)
    {
        return std::rand() % (upper - lower + 1) + lower;
    }
};

} // namespace details

template <class T, RandDistrib D = RandDistrib::Uniform>
T randRange(const T& lower, const T& upper)
{
    return detail::RandRange<T, D>::range(lower, upper);
}

template <RandDistrib D, class T>
T randRange(const T& lower, const T& upper)
{
    return detail::RandRange<T, D>::range(lower, upper);
}

template <class ContT>
const typename ContT::value_type& randElement(const ContT& v)
{
    return v.at(randRange(0UL, v.size() - 1));
}

} // namespace utils
