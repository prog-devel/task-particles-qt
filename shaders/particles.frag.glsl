#version 430 core

in vec2 uv;
in vec4 color;

out vec4 fragColour;

void main()
{
    vec4 col = color;
    // Make particle be opaque at the centre, and transparent towads the edges
    col.a = smoothstep(col.a, 0.0f, abs(uv.x - 0.5f) * 2.0f) * smoothstep(col.a, 0.0f, abs(uv.y - 0.5f) * 2.0f);

    fragColour = col;
}
