#version 430 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in vec2 passSize[];
in vec4 passColor[];
in vec2 passLifetime[];

out vec2 uv;
out vec4 color;

void main()
{
    vec2 lt = passLifetime[0];
    if (lt.x > 0.0f || lt.y <= 0.0f) {
        return;
    }

    vec4 pos = gl_in[0].gl_Position;
    vec2 size = passSize[0];

    float hw = size.x / 2;
    float hh = size.y / 2;

    gl_Position = (pos + vec4(-hw, -hh, 0.0f, 0.0f));
    uv = vec2(0.0f, 0.0f);
    color = passColor[0];
    EmitVertex();

    gl_Position = (pos + vec4(hw, -hh, 0.0f, 0.0f));
    uv = vec2(1.0f, 0.0f);
    color = passColor[0];
    EmitVertex();

    gl_Position = (pos + vec4(-hw, hh, 0.0f, 0.0f));
    uv = vec2(0.0f, 1.0f);
    color = passColor[0];
    EmitVertex();

    gl_Position = (pos + vec4(hw, hh, 0.0f, 0.0f));
    uv = vec2(1.0f, 1.0f);
    color = passColor[0];
    EmitVertex();

    EndPrimitive();
}
