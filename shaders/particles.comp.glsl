#version 430 core
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_storage_buffer_object : enable

layout( local_size_x = 128, local_size_y = 1, local_size_z = 1 ) in;

uniform vec2 gravity;

struct Particle
{
    vec2 lifetime;
    vec2 velocity;
    vec3 pos;
    vec4 color;
    vec2 size;
};

layout ( std140, binding = 0 ) buffer ParticleData_t {
    Particle particles[];
};

// Time since last update
uniform float deltaTime;

void main()
{
    uint gid = gl_GlobalInvocationID.x;
    Particle p = particles[gid];

    // Lifetime logic

    if (p.lifetime.y <= 0.0f) {
        return;
    }

    if (p.lifetime.x > 0.0f) {
        p.lifetime.x -= deltaTime;
    }
    else if (p.lifetime.y > 0.0f) {
        p.lifetime.y -= deltaTime;
    }

    if (p.lifetime.x > 0.0f) {
        particles[gid] = p;
        return;
    }

    // Physics

    p.velocity += gravity * deltaTime;
    p.pos.xy += p.velocity * deltaTime;

    // TODO color,size animations

    particles[gid] = p;
}
