#version 430 core

uniform mat4 viewMat;
uniform mat4 projMat;

layout (location=0) in vec4 inColor;
layout (location=1) in vec3 inPos;
layout (location=2) in vec2 inLifetime;
layout (location=3) in vec2 inSize;

out vec2 passSize;
out vec4 passColor;
out vec2 passLifetime;

void main()
{
    mat4 VP = projMat * viewMat;
    gl_Position = VP * vec4(inPos.xyz, 1.0f);

    passColor = inColor;
    passLifetime = inLifetime;
    passSize = inSize;
}
