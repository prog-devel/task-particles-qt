#pragma once

#include "viewport_item.h"

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/mat4x4.hpp>
#include <QMatrix4x4>

struct ParticleSystemInit
{
    class ShadersGroup;

    explicit ParticleSystemInit(QObject* outer);

    ShadersGroup* m_shaders;

    int m_numParticles = 0;

    // NOTE QMatrix4x4 has inconvenient interface :(
    QMatrix4x4 m_proj{ &glm::ortho(-0.5f, 0.5f, -0.5f, 0.5f, -0.5f, 0.5f)[0][0] };

    QVector2D m_gravity = {0.0f, -9.8f};

    QVector2D m_spawnTimeRange;
    QVector2D m_lifeTimeRange;

    QPointF m_spawnPoint{};

    QVector2D m_spawnDirectionRange{};
    QVector2D m_spawnVelocityRange{};

    std::vector<QColor> m_particleColors;

    QVector2D m_particleSizeRange;
};

class ParticleSystem: public ViewportItem, private ParticleSystemInit
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(ShadersGroup* shaders READ shadersGroup REQUIRED CONSTANT FINAL);
    Q_PROPERTY(int numParticles MEMBER m_numParticles REQUIRED FINAL);

    Q_PROPERTY(QMatrix4x4 projMatrix MEMBER m_proj FINAL)
    Q_PROPERTY(QVector2D gravity MEMBER m_gravity FINAL);

    Q_PROPERTY(QVector2D spawnTimeRange MEMBER m_spawnTimeRange FINAL);
    Q_PROPERTY(QVector2D lifeTimeRange MEMBER m_lifeTimeRange REQUIRED FINAL);

    Q_PROPERTY(QPointF spawnPoint MEMBER m_spawnPoint FINAL);
    Q_PROPERTY(QVector2D spawnDirectionRange MEMBER m_spawnDirectionRange FINAL);
    Q_PROPERTY(QVector2D spawnVelocityRange MEMBER m_spawnVelocityRange FINAL);

    Q_PROPERTY(std::vector<QColor> particleColors MEMBER m_particleColors REQUIRED FINAL);

    Q_PROPERTY(QVector2D particleSizeRange MEMBER m_particleSizeRange REQUIRED FINAL);

    class Impl;

public:
    explicit ParticleSystem(QQuickItem *parent = nullptr);

    const ParticleSystemInit& initialState() const { return *this; }

private:
    ShadersGroup* shadersGroup() const { return m_shaders; }
};

class ParticleSystemInit::ShadersGroup : public QObject
{
    Q_OBJECT
    QML_ANONYMOUS

    Q_PROPERTY(QString vert MEMBER vert REQUIRED)
    Q_PROPERTY(QString geom MEMBER geom REQUIRED)
    Q_PROPERTY(QString frag MEMBER frag REQUIRED)
    Q_PROPERTY(QString comp MEMBER comp REQUIRED)

public:
    using QObject::QObject;

    QString vert;
    QString geom;
    QString frag;
    QString comp;
};
