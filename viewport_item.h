#pragma once

#include <QQuickFramebufferObject>

class QTimer;

class ViewportItem : public QQuickFramebufferObject
{
    Q_OBJECT
    QML_ANONYMOUS

    class RendererInternal;

    Q_PROPERTY(bool started READ isStarted WRITE setStarted NOTIFY onStartedChanged FINAL)
    Q_PROPERTY(QColor bgColor MEMBER m_bgColor FINAL)

public:
    class RendererImpl
    {
    public:
        virtual ~RendererImpl() = default;

        virtual void render(float tDiff) = 0;
        virtual void synchronize() {}
    };

private:
    using impl_factory_t = std::function<std::unique_ptr<RendererImpl>()>;

    ViewportItem(QQuickItem* parent, impl_factory_t impl_factory);

public:
    template <class ImplFactT>
    ViewportItem(QQuickItem* parent, ImplFactT&& implFactory)
        : ViewportItem(parent, impl_factory_t{std::forward<ImplFactT>(implFactory)})
    {}

    bool isStarted() const;
    void setStarted(bool);

public slots:
    void restart();

signals:
    void onStartedChanged();

protected:
    Renderer *createRenderer() const final;

private:
    bool checkStop() const;

    mutable impl_factory_t m_impl_factory;
    QTimer* m_updateTimer;
    mutable bool m_stopRequest = false;

    mutable QColor m_bgColor = {0, 0, 0, 0};
};
