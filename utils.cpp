#include "utils.h"

namespace utils {

float randNormal()
{
    thread_local static std::random_device rd{};
    thread_local static std::mt19937 gen{rd()};
    thread_local static std::normal_distribution d{0.0f};

    return d(gen);
}

} // namespace utils
