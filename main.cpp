#include <cstdio>
#include <cstdlib>
#include <ctime>

#include <QtGlobal>
#include <QOffscreenSurface>
#include <QOpenGLContext>
#include <QOpenGLFunctions>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>

namespace {
void debugOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // FIXME context is null
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stdout, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtInfoMsg:
        fprintf(stdout, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stdout); // TODO remove
        break;
    case QtWarningMsg:
        fprintf(stdout, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stdout);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);
        abort();
    }
}
}

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));

    QGuiApplication app(argc, argv);

    qInstallMessageHandler(debugOutput);

    QQuickWindow::setGraphicsApi(QSGRendererInterface::OpenGL);

    {
        QOffscreenSurface surf;
        surf.create();
        QOpenGLContext ctx;
        ctx.create();
        ctx.makeCurrent(&surf);

        qInfo() << "OpenGL Version Info:" << (const char *)glGetString(GL_VERSION);
        // qDebug() << "OpenGL Extensions:" << (const char*)ctx.functions()->glGetString(GL_EXTENSIONS);
    }

    QQmlApplicationEngine engine;

    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);

    engine.load(u"qrc:/particles/qml/Main.qml"_qs);

    return app.exec();
}
