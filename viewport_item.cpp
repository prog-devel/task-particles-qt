#include "viewport_item.h"

#include <QTimer>
#include <QElapsedTimer>
#include <QOpenGLFunctions>


class ViewportItem::RendererInternal final : public QQuickFramebufferObject::Renderer, protected QOpenGLFunctions
{
public:
    RendererInternal(const ViewportItem* outerItem, impl_factory_t&& impl_factory)
        : m_impl_factory(std::move(impl_factory))
        , m_outerItem(outerItem) {
        initializeOpenGLFunctions();
    }

    void render() override;
    void synchronize(QQuickFramebufferObject *item) override;

private:
    const ViewportItem* m_outerItem;
    impl_factory_t m_impl_factory;
    std::unique_ptr<RendererImpl> m_impl;
    QElapsedTimer m_t;
};


ViewportItem::ViewportItem(QQuickItem* parent, impl_factory_t impl_factory)
    : QQuickFramebufferObject(parent)
    , m_impl_factory(std::move(impl_factory))
    , m_updateTimer(new QTimer(this))
{
    if (!m_impl_factory) {
        throw std::runtime_error("Invalid renderer factory");
    }

    // TODO check performance
    // TODO don't update entire Item (update only FBO?)
    connect(m_updateTimer, &QTimer::timeout, this, &QQuickFramebufferObject::update);
}

void ViewportItem::setStarted(bool yes)
{
    if (isStarted() == yes) {
        return;
    }

    if (yes) {
        // TODO configurable rate
        m_updateTimer->start(10);
    }
    else {
        m_updateTimer->stop();
        m_stopRequest = true;
    }

    emit onStartedChanged();
}

bool ViewportItem::isStarted() const
{
    return m_updateTimer->isActive();
}

void ViewportItem::restart()
{
    setStarted(false);
    setStarted(true);
}

bool ViewportItem::checkStop() const
{
    bool oldStopReq = m_stopRequest;
    m_stopRequest = false;
    return oldStopReq || !isStarted();
}

/// Renderer

QQuickFramebufferObject::Renderer* ViewportItem::createRenderer() const
{
    return new RendererInternal(this, std::move(m_impl_factory));
}

void ViewportItem::RendererInternal::render()
{
    const auto& bg = m_outerItem->m_bgColor;
    glClearColor(bg.redF(), bg.greenF(), bg.blueF(), bg.alphaF());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (m_impl) {
        const auto tDiff = (float)m_t.restart() / 1000.0f;
        m_impl->render(tDiff);
    }
}

void ViewportItem::RendererInternal::synchronize(QQuickFramebufferObject *item)
{
    if (m_outerItem->checkStop()) {
        if (m_impl) {
            m_impl.reset();
            m_t.invalidate();
            // invalidateFramebufferObject();
        }
        return;
    }

    if (!m_impl) {
        try {
            m_impl = m_impl_factory();
        }
        catch (const std::runtime_error& ex) {
            qCritical() << "Failed to create Renderer implementation:" << ex.what();
        }
        catch (...) {
            qCritical() << "Failed to create Renderer implementation:" << "Unknown error";
        }

        m_t.invalidate();
        m_t.start();
    }

    if (m_impl) {
        m_impl->synchronize();
    }
}
