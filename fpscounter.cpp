#include "fpscounter.h"

#include <QTimer>

FPSCounter::FPSCounter(QQuickItem* parent)
    : QQuickItem(parent)
    , m_updateTimer(new QTimer(this))
{
    connect(this, &QQuickItem::windowChanged, this, &FPSCounter::handleWindowChanged);

    connect(m_updateTimer, &QTimer::timeout, this, [this] {
        if (m_newFPS != m_fps) {
            m_fps = m_newFPS;
            emit onFPSChanged();
        }
    });
}

void FPSCounter::handleWindowChanged(QQuickWindow *win)
{
    if (win) {
        connect(win, &QQuickWindow::beforeSynchronizing, this, [this] {
                ++m_count;
                // TODO think about that
                if (m_t.elapsed() > 30) {
                    m_newFPS = (m_newFPS + (qreal)m_count / (qreal)m_t.elapsed() * 1000.0) / 2.0;
                    m_t.restart();
                    m_count = 0;
                }
            }, Qt::DirectConnection);

        m_count = 0;
        m_t.invalidate();
        m_t.start();

        m_updateTimer->start(1000);
    }
    else {
        m_updateTimer->stop();
    }
}
